import React from 'react';
import If from '../Template/If';

export default props => (
    //Se props.hide não for true, vai renderizar o elemento
    <If test={!props.hide}>
            <button className={'btn btn-' + props.style}
                onClick={props.onClick}><i className={'fa fa-' + props.icon}></i>
            </button>
    </If>
)