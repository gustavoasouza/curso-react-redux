import React from 'react';

export default props => {
    //Se props.test for true vai renderizar os filhos do componente If
    if (props.test) {
        return props.children
    } else {
        return false
    }
}