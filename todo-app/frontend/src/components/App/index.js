import '../../styles/css/bootstrap.min.css';

import React from 'react';
import Menu from '../Template/menu';

// Como os comentes Todo e About já estão importados nas rotas, não precisa importar no App
import Routes from '../App/routes';

export default props => (
    <>
        <div className="container">
            <Menu />
            <Routes />
        </div>
    </>
)