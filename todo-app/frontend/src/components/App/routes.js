import React from 'react';
import { Router, Route, Redirect, hashHistory } from 'react-router';

import Todo from '../Todo';
import About from '../About';

export default props => 
    <>
        <Router history={hashHistory}>
            <Route path='/todos' component={Todo} />
            <Route path='/about' component={About} />

            {/* Sempre que uma URL for inválida, redirecionar para /todos */}
            <Redirect from='*' to='/todos' />
        </Router>
    </>