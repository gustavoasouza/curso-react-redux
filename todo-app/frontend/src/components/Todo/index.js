import React, { Component } from './node_modules/react';
import axios from 'axios';

import PageHeader from '../Template/pageHeader';
import TodoForm from './TodoForm';
import TodoList from './TodoList';

const URL = 'http://localhost:3003/api/todos';

export default class Todo extends Component {
    //A função constructor sempre apontará para a classe
    constructor(props) {
        super(props)

        this.state = {
            description: '',
            list: []
        }

        this.handleAdd = this.handleAdd.bind(this)
        this.handleChange = this.handleChange.bind(this)
        this.handleRemove = this.handleRemove.bind(this)

        this.refresh()
    }

    refresh() {
        axios.get(`${URL}?sort=-createdAt`)
            .then(resp => this.state({...this.state, description: '', list: resp.data}))
    }

    handleChange(e) {
        this.state({...this.state, description: e.target.value})
    }

    handleAdd() {
        const description = this.state.description
        axios.post(URL, { description })
            .then(resp => this.refresh())
    }

    handleRemove(todo) {
        axios.delete(`${URL}/${todo._id}`)
            .then(resp => this.refresh())
    }

    render() {
        return (
            <> 
                <PageHeader name="Tarefas" small="Cadastro" />

                //É um componente controlado pelo React (Controlled component), não mais pela DOM
                <TodoForm
                    description={this.state.description} 
                    handleChange={this.handleChange}
                    handleAdd={this.handleAdd} 
                />
                
                <TodoList 
                    list={this.state.list} 
                    handleRemove={this.handleRemove}
                />
            </>
        );
    }
}