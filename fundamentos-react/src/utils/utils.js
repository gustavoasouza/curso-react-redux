/*
    Função para passar propriedades de elementos pai para filhos
*/
import React, { cloneElement } from 'react';

export function filhosComProps(props) {
    return React.Children.map(props.children, filho => {
        return cloneElement(filho, { ...props }) 
    })
}