import React from 'react';
import ReactDOM from 'react-dom';

// import PrimeiroComponente from './components/PrimeiroComponente';

//Nesse caso, deve-se usar exatamente os mesmos nomes que foram exportados
//Porém da pra fazer um apelido ao componente usando o operador 'as'

//Componente A sendo importado como default, e fazendo um destructuring no B
// import CompA, { CompB as B } from './components/DoisComponentes';

// import MultiElementos from './components/MultiElementos';

// import FamiliaSilva from './components/FamiliaSilva';

// import Familia from './components/Familia';
// import Membro from './components/Membro';

// import ComponenteComFuncao from './components/ComponenteComFuncao';

// import Pai from './components/Pai';

// import ComponenteClasse from './components/ComponenteClasse';

// import Contador from './components/Contador';

import Hook from './components/Hook';

ReactDOM.render(
    <>
        <Hook />

        {/* <Contador numero={0} /> */}

        {/* <ComponenteClasse valor="Sou um componente de classe" /> */}

        {/* <Pai /> */}
        {/* <ComponenteComFuncao /> */}

        {/* Inserindo elementos filhos em um elemento pai */}
        {/* <Familia sobrenome="Alves">
            <Membro nome="Gustavo" />
            <Membro nome="Guilherme" />
        </Familia>

        <Familia sobrenome="Souza">
            <Membro nome="Ezequiel" />
            <Membro nome="Alexandre" />
        </Familia> */}

        {/* <FamiliaSilva sobrenome="Silva" /> */}
        {/* <CompA valor="Eu sou o A" />
        <B valor="Eu sou o B" />
        <PrimeiroComponente valor="Bom dia!" /> */}
    </>
    , document.getElementById('root'))