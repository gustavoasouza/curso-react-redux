import React, { Component } from 'react'

export default class Contador extends Component {
    state = {
        numero: 0
    }

    // Forçando o this a apontar para a classe por meio da função construtora

    // 1° Solução (Mais verbosa)
    // constructor(props) {
    //     super(props)

    //     this.maisUm = this.maisUm.bind(this)
    // }
        
    //   Nesse contexto, o construtor() sempre irá apontar para uma instancia 
    //   de Contador.

    //   Com isso estamos apontando o this do método maisUm para o this do Contador.
       

    //2° Solução (Mais prática)
    /*
        Usar uma arrow function para chamar a função.
        Pois o this em uma arrow function aponta para o contexto em que foi executada.
    */


    //3° Transformar a função maisUm em uma função arrow
    /*
        Funções arrow não possuem o próprio this.
        O this na função arrow está se referenciando ao local que ela foi escrita.
        Como nesse caso a arrow function foi escrita dentro da classe Contador,
        o this passa a ser a instancia do Contador
        
    */


    maisUm = () => {
        //Informando somente os atributos que serão mudados
        this.setState({ numero: this.state.numero + 1 })
    }

    menosUm = () => {
        this.setState({ numero: this.state.numero - 1 })
    }

    alterarNumero = diferenca => {
        this.setState({ 
            numero: this.state.numero + diferenca
        })
    }

    render() {
        return (
            <>
                <div>Número: {this.state.numero} </div>
                {/* 
                    Aqui está sendo passado a referência da função, e não a chamada,
                    por isso não está usando parenteses.

                    Usa-se parenteses quando se quer passar algum paramêtro para a função.
                    Ou quando a função tem algum retorno.
                */}
                <button onClick={this.maisUm}>Inc</button>
                <button onClick={this.menosUm}>Dec</button>
                <button onClick={() => this.alterarNumero(10)}>Inc10</button>
                <button onClick={() => this.alterarNumero(-10)}>Dec10</button>
            </>
        )
    }
}