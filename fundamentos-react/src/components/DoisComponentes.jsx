import React from 'react';

/*
    Formas de exportar:

    Exportando multiplos componentes. (Uma boa prática é sempre ter somente um componente por arquivo).

    1° Pode-se colocar export antes de cada componente.
    Ex: export const CompA = props => <h1>Primeiro diz: {props.valor}</h1>

    2° Pode-se exportar todos os componentes de uma vez.
    Ex: export { CompA, CompB }

    3° Pode-se exportar um componente como sendo default.
    Ex: export default const CompA = props => <h1>Primeiro diz: {props.valor}</h1>

    Obs: Quando se usa somente o export, não se pode retornar elementos 
    gerados por funções anonimas, pois não tem como referencia-los. 

    Exportar funções anonimas somente usando o default.
    Ex: export default const CompA = props => <h1>Primeiro diz: {props.valor}</h1> 
*/

const CompA = props => <h1>Primeiro diz: {props.valor}</h1>

//Exportando o componente B
export const CompB = props => <h1>Segundo diz: {props.valor}</h1>

//Exportando somente o componente A como default
export default CompA