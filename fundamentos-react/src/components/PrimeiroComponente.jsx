/*
    Criando um componente funcional (Função)

    - Um componente funcional é instaceado a partir de uma função contrutora.
    - Um componente de classe é instanceado a partir de uma classe.

*/

import React from 'react';

//Criando um componente com função normal
function primeiro() {
    return <h1>Primeiro Componente!</h1>
}

//Criando um componente com arrow function
export default (props) => 
    <>
        <h1>{ props.valor }</h1>
    </>