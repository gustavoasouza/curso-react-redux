import React, { cloneElement } from 'react';
import { filhosComProps } from '../utils';
/*
    Renderizando os elementos filhos de um elemento pai por meio da propriedade children.
    
    Para acessar os elementos filhos, basta usar props.children
*/
export default props => 
    <div>

        <h1>Família</h1>
        {/* Renderizando todos os elementos que foram passado como filhos de uma tag */}

        {/* 
            Somente para um único componente

            cloneElement() - Clona os filhos recebidos a partir disso
            pode-se passar propriedades pro clone gerado
            
            1° parâmetro - É o proprio elemento que está sendo clonado.
            2° parâmetro - São as propriedades que serão passadas.
        
        */}
        
        {/* 
            Passando o valor da propriedade sobrenome de pai para um unico elemento filho 
            
            Forma menos usada

            {cloneElement(props.children, {
            sobrenome: props.sobrenome
        })}
        */}

        {/* 
            Forma mais usada

            Espalhando os valores de props dentro do objeto clonado,
            por meio do spread.

            {cloneElement(props.children, { ...props })}
        */}

        {/* Resolução do problema feita por mim */}
        {/* {props.children.map((membro => {
            console.log(membro.props.nome, props.sobrenome)
            return createElement('p', {}, membro.props.nome + ' ' + props.sobrenome)
        }))} */}

        {/* 
            Resolução do problema feita pelo professor 

            O React possúi uma API para manipulação de children. React.Children.
            Nela temos uma função muito utilitária que é a React.Children.map,
            ela funciona igual ao Array.map, a diferênça é que ela funciona também com
            funções, objetos ou qualquer coisa passada como elemento filho.

            {React.Children.map(props.children, filho => {
            return cloneElement(filho, { ...props }) 
        })}
        */}

        {filhosComProps(props)}


    </div>