import React, { Component } from 'react';

export default class ComponenteClasse extends Component {
    render() {
        return (
            <>
                {/* Caso não tenha o primeiro valor, ele definirá como padrão */}
                <h1> {this.props.valor || 'Padrão'} </h1>
            </>
        )
    }
}
