import React from 'react';

import Membro from './Membro';

export default props => 
    <div>
        {/* 
            Componentes funcionais são funções construtoras que instancearão elementos.

            Por baixo dos panos é isso que acontece:
            function Membro({nome, sobrenome}) {}
            new Membro('Rafael', 'Silva')
        
        */}

        <Membro nome="Rafael" sobrenome={props.sobrenome} />
        <Membro nome="Daniela" sobrenome={props.sobrenome} />
        <Membro nome="Pedro" sobrenome={props.sobrenome} />
        <Membro nome="Ana" sobrenome={props.sobrenome} />
    </div>