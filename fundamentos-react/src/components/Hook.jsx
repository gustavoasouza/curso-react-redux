/*
    React Hooks

    Por padrão componentes funcionais não possuem estados.
    Porme a partir da versão 16.8 do React foi implementado os Hooks,
    que permitem que componentes funcionais tenham estados.
*/

import React, { useState, useEffect } from 'react';

export default props => {
    //Primeiro paramêtro é o valor, o segundo é a função que irá alterar o valor.
    //O paramêtro do useState indica o valor inicial da variavel
    const [contador, setContador] = useState(100)
    const [status, setParOuImpar] = useState('Par')

    /*
        useEffect implica no ciclo de vida do componente.

        Sempre que o componente for atualizado, ou modificado,
        o useEffect será executado.
    */
    useEffect(() => {
        contador % 2 === 0 ? setParOuImpar('Par') : setParOuImpar('Impar')
    })
    
    return (
        <> 
            <h1>{contador}</h1>
            <h3>{status}</h3>
            <button onClick={() => setContador(contador++)}>Inc</button>
            <button onClick={() => setContador(contador--)}>Dec</button>
        </>
    )
}