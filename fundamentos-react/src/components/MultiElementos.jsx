import React from 'react'

/*
    Sempre ao exibir multiplos componentes, eles devem estar 
    envolvidos por alguma tag ou o fragment. <> </>

    É uma boa prática usar o fragment, pois se fosse colocado em volta
    de uma div, na hora de renderizar, iria ter uma div a mais no browser.
    Já usando fragment, o mesmo não é renderizado.

    Além de usar o fragment, é possivel retornar os elementos por meio de um
    array de elementos, separando os mesmos por vírgula.
*/

export default props =>
    <>
        <h1>Parte 1</h1>
        <h1>Parte 2</h1>
    </>